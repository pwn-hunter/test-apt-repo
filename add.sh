#!/bin/bash

# Remove all old packages
reprepro -V removefilter test 'Section'

# Add new packages
reprepro --ignore=forbiddenchar -S main -P extra includedeb test ../debs-all-test/*.deb
